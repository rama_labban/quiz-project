import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './components/main/main.component';

const routes: Routes = [
  { path: '', redirectTo: '/users', pathMatch: 'full' },
  {
   path: "",
   component: MainComponent,
   children: [
     {
       path: "",
       loadChildren: () => import ("./components/main/main.module").then(m => m.MainModule)
     }
   ]
 },
 { path: '**', redirectTo: '/users' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
