import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";

@NgModule({
  declarations: [],

  imports: [
    CommonModule
  ],

  exports:[
    NgxSpinnerModule
  ],
})
export class NgxModule { }
