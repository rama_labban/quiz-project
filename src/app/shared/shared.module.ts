import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { CardComponent } from './components/card/card.component';
import { NgxModule } from './modules/ngx.module';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './modules/material.module';
import { FlatCardComponent } from './components/flat-card/flat-card.component';



@NgModule({
  declarations: [
    HeaderComponent,
    CardComponent,
    FlatCardComponent
  ],
  exports:[
    HeaderComponent,
    FlatCardComponent,
    HttpClientModule,
    CardComponent,
    NgxModule,
    MaterialModule

  ],
  imports: [
    HttpClientModule,
    CommonModule,
    RouterModule,
    NgxModule,
    MaterialModule
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class SharedModule { }
