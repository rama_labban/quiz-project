import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/core/models/user';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() user: User;
  constructor(
    private _router: Router,

  ) {

  }

  ngOnInit(): void {
  }

  navigateToUser() {
    this._router.navigate(['/users' , this.user.id]);
  }
}
