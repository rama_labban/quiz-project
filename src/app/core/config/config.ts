import { HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MyAppConfig } from '../models/my-config';

let base_url: string;
let httpOptions: any;

base_url = environment.apiUrl;

export const AppConfig: MyAppConfig = {
  baseUrl: environment.apiUrl,
  paths: {
    users: "users",
    user: "users/"
  },
  httpHeaders: httpOptions = {
    headers: new HttpHeaders({
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
      'Content-Type':  'application/json',

    })}
}

