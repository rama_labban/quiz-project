import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Injectable } from '@angular/core';
import { AppConfig } from '../config/config';
import { MyAppConfig } from '../models/my-config';

@Injectable({
  providedIn: 'root'
})
export class UserService {
   config: MyAppConfig = AppConfig;

  constructor(
    private _http: HttpClient
  ) {

  }

  getAllUsers(page: number){
   return this._http.get(this.config.baseUrl + this.config.paths.users + "?page=" + page, this.config.httpHeaders);
  }
  getUSerById(userId: number){
    return this._http.get(this.config.baseUrl + this.config.paths.user + userId, this.config.httpHeaders);
  }
}
