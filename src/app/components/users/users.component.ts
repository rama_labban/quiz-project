import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from 'src/app/core/models/user';
import { UserService } from 'src/app/core/services/user.service';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  userList: User [] = [];
  tempList: User [] = [];
  pageSize: number = 0;
  total_pages: number = 0;
  total: number = 0;
  isLoaded: boolean = false;

  constructor(
    private _userService: UserService,
    private _spinner: NgxSpinnerService,
    private _snackbarService: SnackbarService
    ){

     }

  ngOnInit(): void {
    this.getUsers(1)
  }

  getUsers(page:number){
    this._spinner.show();
    this.isLoaded = false;
    this._userService.getAllUsers(page)
    .subscribe((response: any) => {
      this.pageSize = response.per_page;
      this.total_pages = response.total_pages;
      this.total = response.total;
      this.userList = response.data;
      this.tempList = this.userList;
    }, (error) => {
      this._spinner.hide();
      this._snackbarService.error('An error has occured');
    }, ()=> {
      this._spinner.hide();
      this.isLoaded =true;
    this._snackbarService.info('The Fetch is Completed');

  })
  }

  onChangePage(event){
   console.log(event);
    this.getUsers(event.pageIndex + 1)
  }

  filterUsers(value){
    if(value){
    this.userList = this.userList.filter(user => user.id == value );
    console.log(this.userList);}
    else{
    this.userList = this.tempList;
    }
  }

}
