import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { User } from 'src/app/core/models/user';
import { UserService } from 'src/app/core/services/user.service';
import { SnackbarService } from 'src/app/shared/services/snackbar.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  userID: number;
  user: User = new User();
  constructor(
    private route: ActivatedRoute,
    private _userService: UserService,
    private _spinner: NgxSpinnerService,
    private _snackbarService: SnackbarService
  ) {
    this.route.params.subscribe(params => this.userID = params['id'])
  }

  ngOnInit(): void {
    this.getUser();
  }
  getUser(){
    this._spinner.show();
    this._userService.getUSerById(this.userID)
    .subscribe((response: any) => {
      this.user = response.data;
    }, (error) =>{
            this._snackbarService.error('An error has occured');
    }, () =>{
      this._spinner.hide();

      this._snackbarService.info('The Fetch is Completed');
    }
    )
  }
}
